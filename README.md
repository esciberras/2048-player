# README #

2048 player is an assignment where i had to build an algorithm to play the game 2048. 
I was provided with code for the game itself so my work can be found in "ai.c". 
This project encouraged me to read and understand other people's code and use debugging tools such as gdb.

### How do I get set up? ###

* clone repositiory 'git clone < Repo Address >  '
* run 'make all'
* this creates an executable named 2048

### Usage ###

To play the game yourself

'./2048'

To run the game playing algorithm

'./2048 ai <max/avg> <max_depth> slow'

e.g. 

'./2048 ai avg 9 slow'

Where:

* <max/min> tells the ai to move according to what gives the higest score or average score
* <max_depth> is how far ahead the ai should look in its decision making (7-10 is a sweet spot for accuracy and speed)
* slow is an optional flag used to see the ai play at a reasonable pace



 

