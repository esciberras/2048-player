/* Author: Eric Sciberras */

#include <time.h>
#include <stdlib.h>
#include "ai.h"
#include "utils.h"
#include "priority_queue.h"
#include "math.h"
#include "inttypes.h"

#include <assert.h>


struct heap h;

void initialize_ai(){
	heap_init(&h);
}

/**
 * Find best action by building all possible paths up to depth max_depth
 * and back propagate using either max or avg
 */

struct ai_s get_next_move( uint8_t board[SIZE][SIZE], int maxDepth, propagation_t propagation ){

	int i=0,numNodes=0;
	struct ai_s aiOutput; 
	aiOutput.generated=0;
	aiOutput.expanded=0;
	int scores[4]={0,0,0,0}; //this array holds the scores for each move choice and is used to decide how to play
	int exploredSize=1000; // keeps track of the size of our explored array
	move_t move=0;
	node_t* node=(node_t*)calloc(1,sizeof(node_t));
	node_t* newNode=(node_t*)calloc(1,sizeof(node_t)); // 
	node_t* tempNode=(node_t*)calloc(1,sizeof(node_t)); // temporary stores node when executing moves
	node_t** explored=(node_t**)calloc(exploredSize,sizeof(node_t)); // holds all the nodes we have looked at

	//just in case
	assert(node);
	assert(newNode);
	assert(tempNode);
	assert(explored);

	memcpy(node->board,board,sizeof(uint8_t) * SIZE * SIZE); // put board in node
	node->move=-1;
	//heap_init(&h);
	array_add(node,explored,numNodes);
	//heap_push(&h,explored[numNodes]);
	numNodes++;
	//frontier_add(node);


	while( (&h)->count != 0 ){
		node=heap_delete(&h);
		aiOutput.expanded++;

		//array_add(node,explored,numNodes);
		//numNodes++;
		if(node->depth < maxDepth){
			for(i=0;i<4;i++){
				aiOutput.generated++;
				// This Block handles the assignment of the Newnode (new board) 
				// and node (board before the move is made)
				memcpy(tempNode,node, sizeof(node_t)); 
				execute_move_t( node->board, &(node->priority), i);
				memcpy(newNode,node,sizeof(node_t));
				memcpy(node,tempNode,sizeof(node_t));

				// if the new board is different then we will add it to the priority queue and 
				// trace back the score to the first move made, then we will use this data to help 
				// our propagation
				if( board_different(node,newNode) == true ){
					newNode->move = i;
					newNode->depth = node->depth+1;
					newNode->parent = node;
					node->num_childs++;
					move=propogateBackToFirstAction(newNode);

					if( exploredSize <= numNodes){
						explored=(node_t**)realloc(explored,2*exploredSize*sizeof(node_t));
						assert(explored);
						exploredSize = 2*exploredSize;
					}
					array_add(newNode,explored,numNodes);
					numNodes++;

					if(propagation==max){
						if( newNode->priority > scores[move]){
							scores[move]=newNode->priority;
						}
					}
					else{ // Average propogation heuristics: very simple heuristic 
						  // prioritises the scores closer to the current board as they are
						  // more liekly to be acheived due to the random tiles popping up after each turn
						scores[move]+= newNode->priority/ (float)(newNode->depth);
					}
				}	
			}
		}
	}

	for(i=0;i<numNodes;i++){
		free(explored[i]);
	}
	free(explored);
	free(newNode);
	free(tempNode);
	//free(node); // yes i know this 'should' be run but it seems to completely destroy my code
	aiOutput.move= get_max(scores);
	return (aiOutput);
}


void array_add(node_t* node, node_t** explored, int numNodes ){
	node_t* tempNode=(node_t*)calloc(1,sizeof(node_t));
	assert(tempNode);
	memcpy(tempNode,node, sizeof(node_t)); 
	explored[numNodes] = tempNode;
	heap_push(&h,tempNode);
}

//board_different takes two nodes and compares them, it returns True if the boards are different
// or false if otherwise
bool board_different(node_t* node1, node_t* node2){
	int i ,j;
	for(i=0;i<SIZE;i++){
		for(j=0;j<SIZE;j++){
			if(node1->board[i][j]!=node2->board[i][j] ){
				return true;
			}
		}
	}
return false;
}

// get_max takes an int array (4 elements) and find the index of the maximum value,
// in the case of a tie a maximum random move is chosen
int get_max(int* scores){
	int i=0, max=0, matches=0;
	int  duplicate_scores[4]={-1,-1,-1,-1};
	// get the maximum element  
	for (i=0;i<4;i++){
    	if (scores[i] >= max){
        	max = scores[i];
   		}
	}
	// look for duplicates
	for(i=0;i<4;i++){
		if(max==scores[i]){
			duplicate_scores[matches]=i;
			matches++;
		}
	}
	return(duplicate_scores[rand() % matches]);
}

// propogateBackToFirstAction takes a node and finds what original move led to it
int propogateBackToFirstAction(node_t* node){
	if(node->depth == 1){
		return (node->move);
	}
	return(propogateBackToFirstAction(node->parent));
}
