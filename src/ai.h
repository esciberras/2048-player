#ifndef __AI__
#define __AI__

#include <stdint.h>
#include <unistd.h>
#include "node.h"
#include "priority_queue.h"


void initialize_ai();

struct ai_s get_next_move( uint8_t board[SIZE][SIZE], int max_depth, propagation_t propagation );

int get_max(int* scores);

int propogateBackToFirstAction(node_t* node);

bool board_different(node_t* node1, node_t* node2);

void array_add(node_t* node, node_t** explored, int numNodes );

#endif
